
function HTMLActuator() {
  this.tileContainer    = document.getElementsByClassName("tile-container")[0];
  this.scoreContainer   = document.getElementsByClassName("score-container")[0];
  this.messageContainer = document.getElementsByClassName("game-message")[0];

  this.score = 0;
}

HTMLActuator.prototype.actuate = function (grid, metadata) {
  var self = this;

  window.requestAnimationFrame(function () {
    self.clearContainer(self.tileContainer);

    grid.cells.forEach(function (column) {
      column.forEach(function (cell) {
        if (cell) {
          self.addTile(cell);
        }
      });
    });

    self.updateScore(metadata.score);

    if (metadata.over) self.message(false); // You lose
    if (metadata.won) self.message(true); // You win!
  });
};

HTMLActuator.prototype.restart = function () {
  this.clearMessage();
};

HTMLActuator.prototype.clearContainer = function (container) {
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
};

HTMLActuator.prototype.addTile = function (tile) {
  var self = this;

  var element   = document.createElement("div");
  var position  = tile.previousPosition || { x: tile.x, y: tile.y };
  positionClass = this.positionClass(position);

  // resize long texts.
  let isText = true;
  let i = String(tile.value).length;
  while (i--) {
    const c = String(tile.value).charAt(i);
    if ('abcdefghijklmnopqrstuvwxyz.1234567890'.indexOf(c.toLowerCase()) === -1)
      isText = false;
  }
  element.textContent = tile.value;
  let textClass = '';
  if (isText) {
    if (String(tile.value).length > 3) textClass = 'text-3';
    if (String(tile.value).length > 5) textClass = 'text-5';
    if (String(tile.value).length > 8) textClass = 'text-8';
    if (String(tile.value).length > 12) textClass = 'text-12';
    if (String(tile.value).length > 15) textClass = 'text-15';
    if (String(tile.value).length > 20) textClass = 'text-20';
    if (String(tile.value).toLowerCase().substr(0,4) === 'abcd') {
      if (String(tile.value).length > 3) element.style='word-wrap: break-word; display: block; line-height: 60%;';
      if (String(tile.value).length > 8) element.style='word-wrap: break-word; display: block; line-height: 60%;';
      if (String(tile.value).length > 12) element.style='word-wrap: break-word; display: block; line-height: 60%;';
      if (String(tile.value).length > 20) element.style='word-wrap: break-word; display: block; line-height: 60%;';
      element.innerHTML = '<p>' + tile.value + '</p>';
    }
  }

  // We can't use classlist because it somehow glitches when replacing classes
  const tv = String(tile.value).replace('.', 'dot');
  const classes = ["tile", "tile-" + tv, positionClass];
  if (textClass !== '') classes.push(textClass)
  this.applyClasses(element, classes);

  if (tile.previousPosition) {
    // Make sure that the tile gets rendered in the previous position first
    window.requestAnimationFrame(function () {
      classes[2] = self.positionClass({ x: tile.x, y: tile.y });
      self.applyClasses(element, classes); // Update the position
    });
  } else if (tile.mergedFrom) {
    classes.push("tile-merged");
    this.applyClasses(element, classes);

    // Render the tiles that merged
    tile.mergedFrom.forEach(function (merged) {
      self.addTile(merged);
    });
  } else {
    classes.push("tile-new");
    this.applyClasses(element, classes);
  }

  // Put the tile on the board
  this.tileContainer.appendChild(element);
};

HTMLActuator.prototype.applyClasses = function (element, classes) {
  element.setAttribute("class", classes.join(" "));
};

HTMLActuator.prototype.normalizePosition = function (position) {
  return { x: position.x + 1, y: position.y + 1 };
};

HTMLActuator.prototype.positionClass = function (position) {
  position = this.normalizePosition(position);
  return "tile-position-" + position.x + "-" + position.y;
};

HTMLActuator.prototype.updateScore = function (score) {
  this.clearContainer(this.scoreContainer);

  var difference = score - this.score;
  this.score = score;

  this.scoreContainer.textContent = this.score;

  if (difference > 0) {
    var addition = document.createElement("div");
    addition.classList.add("score-addition");
    addition.textContent = "+" + difference;

    this.scoreContainer.appendChild(addition);
  }
};

HTMLActuator.prototype.message = function (won) {
  var type    = won ? "game-won" : "game-over";
  var message = won ? "You win!" : "Game over!"

  // if (ga) ga("send", "event", "game", "end", type, this.score);

  this.messageContainer.classList.add(type);
  this.messageContainer.getElementsByTagName("p")[0].textContent = message;
};

HTMLActuator.prototype.clearMessage = function () {
  this.messageContainer.classList.remove("game-won", "game-over");
};