function GameManager(size, InputManager, Actuator) {
  this.size = size; // Size of the grid
  this.inputManager = new InputManager;
  this.actuator = new Actuator;

  this.games = 0;
  this.moves = 0;
  this.mode = '';
  this.modes = ['+1', '1s', 's1', '🐤', '💿', '👀', 'letters', 'bait', '👶🏻'];

  this.startTiles = 2;

  this.inputManager.on("move", this.move.bind(this));
  this.inputManager.on("restart", this.restart.bind(this));

  this.setup();
}

// Restart the game
GameManager.prototype.restart = function () {
  this.games += 1;
  this.actuator.restart();
  this.setup();
};

// Set up the game
GameManager.prototype.setup = function () {
  if (this.games === 0) {
    this.mode = 'intro';
  } else if (this.games === 1) {
    this.mode = 'intro2';
  } else if (this.games === 2) {
    this.mode = 'intro3';
  } else if (this.modes.length > 0) {
    const i = Math.floor(Math.random() * this.modes.length);
    this.mode = this.modes[i];
    this.modes.splice(i, 1);
  } else {
    this.mode = 'end';
  }

  this.grid = new Grid(this.size);

  this.moves = 0;
  this.score = 0;
  this.over = false;
  this.won = false;

  // Add the initial tiles
  this.addStartTiles();

  // Update the actuator
  this.actuate();
};

// Set up the initial tiles to start the game with
GameManager.prototype.addStartTiles = function () {
  if (this.mode === '1s') {
    for (var i = 0; i < (this.size * this.size); i++) {
      this.addRandomTile();
    }
  } else if (this.mode === 'end') {
    for (var i = 0; i < (this.size * (this.size - 1)); i++) {
      this.addRandomTile();
    }
  } else {
    for (var i = 0; i < this.startTiles; i++) {
      this.addRandomTile();
    }
  }
};

// Adds a tile in a random position
GameManager.prototype.addRandomTile = function () {
  if (this.grid.cellsAvailable()) {
    if (this.mode === 'intro') {
      this.introAddRandomTile(1);
    } else if (this.mode === 'intro2') {
      this.introAddRandomTile(2);
    } else if (this.mode === 'intro3') {
      this.introAddRandomTile(3);
    } else if (this.mode === '1s') {
      this.onesAddRandomTile();
    } else if (this.mode === 's1') {
      this.introAddRandomTile(1);
    } else if (this.mode === '+1') {
      this.plusoneAddRandomTile();
    } else if (this.mode === 'letters') {
      this.lettersAddRandomTile();
    } else if (this.mode === 'bait') {
      this.baitAddRandomTile();
    } else if (this.mode === '🐤') {
      this.chickenAddRandomTile();
    } else if (this.mode === '💿') {
      this.cdAddRandomTile();
    } else if (this.mode === '👀') {
      this.aloneAddRandomTile();
    } else if (this.mode === '👶🏻') {
      this.babyAddRandomTile();
    } else if (this.mode === 'end') {
      this.endAddRandomTile();
    } else {
      // Not implemented
    }
  }
};

// Sends the updated grid to the actuator
GameManager.prototype.actuate = function () {
  this.actuator.actuate(this.grid, {
    score: this.score,
    over: this.over,
    won: this.won
  });
};

// Save all tile positions and remove merger info
GameManager.prototype.prepareTiles = function () {
  this.grid.eachCell(function (x, y, tile) {
    if (tile) {
      tile.mergedFrom = null;
      tile.savePosition();
    }
  });
};

// Move a tile and its representation
GameManager.prototype.moveTile = function (tile, cell) {
  this.grid.cells[tile.x][tile.y] = null;
  this.grid.cells[cell.x][cell.y] = tile;
  tile.updatePosition(cell);
};

// Move tiles on the grid in the specified direction
GameManager.prototype.move = function (direction) {
  // 0: up, 1: right, 2:down, 3: left
  var self = this;

  if (this.over || this.won) return; // Don't do anything if the game's over

  var cell, tile;

  var vector = this.getVector(direction);
  var traversals = this.buildTraversals(vector);
  var moved = false;

  // Save the current tile positions and remove merger information
  this.prepareTiles();

  // Traverse the grid in the right direction and move tiles
  traversals.x.forEach(function (x) {
    traversals.y.forEach(function (y) {
      cell = { x: x, y: y };
      tile = self.grid.cellContent(cell);

      if (tile) {
        var positions = self.findFarthestPosition(cell, vector);
        var next = self.grid.cellContent(positions.next);

        // Only one merger per row traversal?
        if (next && tile.merge(next, self.mode) !== false && !next.mergedFrom) {
          var merged = new Tile(positions.next, tile.merge(next, self.mode));
          merged.mergedFrom = [tile, next];

          self.grid.insertTile(merged);
          self.grid.removeTile(tile);

          // Converge the two tiles' positions
          tile.updatePosition(positions.next);

          // Update the score
          if (self.mode === '+1' && self.grid.hasTile(69)) {
            self.score = 'Nice';
            self.won = true;
          } else if (self.mode === '👶🏻' && self.grid.hasTile('⚰')) {
            self.score = 'RIP';
            self.over = true;
          } else if (self.mode === 'bait' && self.grid.hasTile(2046.999999999999)) {
            self.score = 'Not quite...';
            self.over = true;
          } else if (self.mode === 'letters') {
            self.score = self.grid.longest(true);
          } else {
            self.score += merged.scoreValue;
          }

          // The mighty 2047 tile
          if (merged.scoreValue === 2047) self.won = true;
          if (String(merged.value).toLowerCase() === 'abcdefghijklmnopqrstuvwxyz') self.won = true;
        } else {
          self.moveTile(tile, positions.farthest);
        }
        if (!self.positionsEqual(cell, tile)) {
          moved = true; // The tile moved from its original cell!
        }
      }
    });
  });

  if (moved) {
    if (self.mode === 'end') {
      self.score = 2047;
      self.addRandomTile();
      self.addRandomTile();
      self.addRandomTile();
    }
    this.moves += 1;
    this.addRandomTile();
    if (this.mode === '🐤' && this.grid.hasTile('🍗')) this.over = true;
    if (this.mode === '👀' && this.moves > 25) this.over = true;

    if (!this.movesAvailable()) {
      this.over = true; // Game over!
    }

    this.actuate();
  }
};

// Get the vector representing the chosen direction
GameManager.prototype.getVector = function (direction) {
  // Vectors representing tile movement
  var map = {
    0: { x: 0, y: -1 }, // up
    1: { x: 1, y: 0 },  // right
    2: { x: 0, y: 1 },  // down
    3: { x: -1, y: 0 }   // left
  };

  return map[direction];
};

// Build a list of positions to traverse in the right order
GameManager.prototype.buildTraversals = function (vector) {
  var traversals = { x: [], y: [] };

  for (var pos = 0; pos < this.size; pos++) {
    traversals.x.push(pos);
    traversals.y.push(pos);
  }

  // Always traverse from the farthest cell in the chosen direction
  if (vector.x === 1) traversals.x = traversals.x.reverse();
  if (vector.y === 1) traversals.y = traversals.y.reverse();

  return traversals;
};

GameManager.prototype.findFarthestPosition = function (cell, vector) {
  var previous;

  // Progress towards the vector direction until an obstacle is found
  do {
    previous = cell;
    cell = { x: previous.x + vector.x, y: previous.y + vector.y };
  } while (this.grid.withinBounds(cell) &&
    this.grid.cellAvailable(cell));

  return {
    farthest: previous,
    next: cell // Used to check if a merge is required
  };
};

GameManager.prototype.movesAvailable = function () {
  return this.grid.cellsAvailable() || this.tileMatchesAvailable() || this.mode === 'end';
};

// Check for available matches between tiles (more expensive check)
GameManager.prototype.tileMatchesAvailable = function () {
  var self = this;

  var tile;

  for (var x = 0; x < this.size; x++) {
    for (var y = 0; y < this.size; y++) {
      tile = this.grid.cellContent({ x: x, y: y });

      if (tile) {
        for (var direction = 0; direction < 4; direction++) {
          var vector = self.getVector(direction);
          var cell = { x: x + vector.x, y: y + vector.y };

          var other = self.grid.cellContent(cell);
          if (other) {
          }

          if (other && other.merge(tile, this.mode) !== false) {
            return true; // These two tiles can be merged
          }
        }
      }
    }
  }

  return false;
};

GameManager.prototype.positionsEqual = function (first, second) {
  return first.x === second.x && first.y === second.y;
};