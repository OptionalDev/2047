function Tile(position, value) {
  this.x                = position.x;
  this.y                = position.y;
  this.value            = value || 2;
  this.scoreValue       = value;
  if (isNaN(this.scoreValue)) {
    this.scoreValue = 1;
    if (this.value === '🍗') this.scoreValue = -1;
    if (this.value === '💿') this.scoreValue = 3;
    if (this.value === '📀') this.scoreValue = 10;
    if (this.value === '🐒') this.scoreValue = 200;
  }

  this.previousPosition = null;
  this.mergedFrom       = null; // Tracks tiles that merged together
}

Tile.prototype.savePosition = function () {
  this.previousPosition = { x: this.x, y: this.y };
};

Tile.prototype.updatePosition = function (position) {
  this.x = position.x;
  this.y = position.y;
};

Tile.prototype.merge = function (otherTile, mode) {
  if (mergeResult(this.value, otherTile.value, mode) === false) {
    return mergeResult(otherTile.value, this.value, mode)
  }
  return mergeResult(this.value, otherTile.value, mode);
}

let mergeResult = (a, b, mode) => {
  if (mode === 'end')
    return false;
  if (mode === 'intro3')
    return introthreeMergeResult(a,b);
  if (mode === '+1')
    return plusoneMergeResult(a,b);
  if (mode === '1s')
    return onesMergeResult(a,b);
  if (mode === 's1')
    return soneMergeResult(a,b);
  if (mode === 'letters')
    return lettersMergeResult(a,b);
  if (mode === 'bait')
    return baitMergeResult(a,b);
  if (mode === '🐤')
    return chickenMergeResult(a,b);
  if (mode === '💿')
    return cdMergeResult(a,b);
  if (mode === '👶🏻')
    return babyMergeResult(a,b);
  if (a === b)
    return a + a;
  return false;
}