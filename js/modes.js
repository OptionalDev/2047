// 'intro' & 'intro2'
GameManager.prototype.introAddRandomTile = function (round) {
  if (round === 1) {
    let value = 2;
    if (this.moves > 2) {
      const fancyTiles = [1, 5, 7, 9, 11, 13, 27, 22];
      value = fancyTiles[Math.floor(Math.random() * fancyTiles.length)];
    }
    let tile = new Tile(this.grid.randomAvailableCell(), value);
  
    this.grid.insertTile(tile);
  } else if (round === 2) {
    let value = 2;
    if (this.moves > 0) {
      const fancyTiles = [10, 5, 7, 9, 11, 13, 27, 22, 23];
      value = fancyTiles[Math.floor(Math.random() * fancyTiles.length)];
    }
    if (this.moves === 12) {
      value = 2048;
    }
    let tile = new Tile(this.grid.randomAvailableCell(), value);
  
    this.grid.insertTile(tile);
    if (this.moves === 0) {
      this.moves += 1;
    }
  } else if (round === 3) {
    let value = 2;
    if (this.moves > 0) {
      const fancyTiles = [1, 1, 1, 7.4, 8.5, 10.1, 16.02, 17, 23, 76, 99, 420];
      value = fancyTiles[Math.floor(Math.random() * fancyTiles.length)];
    }
    if (this.moves === 14) {
      value = '😎';
    }
    let tile = new Tile(this.grid.randomAvailableCell(), value);
  
    this.grid.insertTile(tile);
  }
};

let introthreeMergeResult = (a,b) => {
  if (a==='😎') return false;
  if (a===b) return a+a;
  if (a===1) return b+1;
  return false;
}

// '+1'
GameManager.prototype.plusoneAddRandomTile = function () {
    if (this.moves > 0 && this.grid.hasTile(1) || this.grid.hasTile(69)) {
      return
    }
    let tile = new Tile(this.grid.randomAvailableCell(), 1);
    this.grid.insertTile(tile);
};

let plusoneMergeResult = (a,b) => {
  if (a===1) return b+1;
  return false;
}

// 'bait'
GameManager.prototype.baitAddRandomTile = function () {
    let tile = new Tile(this.grid.randomAvailableCell(), 127.9375);
    this.grid.insertTile(tile);
};

let baitMergeResult = (a,b) => {
  if (a===1023.5 && a===b) return 2046.999999999999;
  if (a===b) return a+a;
  return false;
}

// 's1'
GameManager.prototype.soneAddRandomTile = function () {
    let value = 2;
    if (this.moves > 0) {
      const fancyTiles = [1, 1, 1, 11, 11, 11, 5, 7, 9, 13, 27, 22];
      value = fancyTiles[Math.floor(Math.random() * fancyTiles.length)];
    }
    let tile = new Tile(this.grid.randomAvailableCell(), value);
  
    this.grid.insertTile(tile);
}

let soneMergeResult = (a,b) => {
  if (a===1) return Number(b+'1');
  if (a===11) return Number(b+'11');
  if (a===111) return Number(b+'111');
  if (a===b) return a+a;
  return false;
}

// 'letters'
GameManager.prototype.lettersAddRandomTile = function () {
  const abc = 'abcdefghijklmnopqrstuvwxyz';
  let max = this.grid.longest() + 1;
  let value = abc[(Math.floor(Math.random() * (max))) % abc.length];
  if (max > 5) {
    value = abc[(Math.floor(Math.random() * 3) + max - 3) % abc.length];
  }
  // Distraction
  if (this.moves > 2)
    value = Math.random() < 0.03 ?  ['⛔','🙅🏻‍♀️','🙅🏻‍♂','❌','✖','❎'][Math.floor(Math.random() * 6)] : value;
  if (this.grid.longest() === 25) {
    value = 'z'
    if (this.grid.hasTile('z')) value = ['⛔','🙅🏻‍♀️','🙅🏻‍♂','❌','✖','❎'][Math.floor(Math.random() * 6)];
  }
  let tile = new Tile(this.grid.randomAvailableCell(), value);
  this.grid.insertTile(tile);
}

let flipCase = (val, part) => {
  let flipped = '';
  for (let i = 0; i < val.length; i++) {
    let char = val[i];
    if (part.toLowerCase().indexOf(char.toLowerCase()) > -1) {
      if (char.toLowerCase() === char) {
        char = char.toUpperCase();
      } else {
        char = char.toLowerCase();
      }
    }
    flipped += char;
  }
  return flipped;
}

let lettersMergeResult = (a,b) => {
  if (['⛔','🙅🏻‍♀️','🙅🏻‍♂','❌','✖','❎'].indexOf(a) > -1) {
    if (a===b) return a;
    return false;
  }
  if (['⛔','🙅🏻‍♀️','🙅🏻‍♂','❌','✖','❎'].indexOf(b) > -1) return false;
  const al = a.toLowerCase();
  const bl = b.toLowerCase();
  if (al.indexOf(bl) > -1) return flipCase(a, b);
  const abc = 'abcdefghijklmnopqrstuvwxyz';
  const i = abc.indexOf(al);
  if (abc[i + a.length] === bl[0]) return a+b;
  if (al.indexOf(bl[0]) > -1) {
    // partial overlap
    let i = 0;
    for (i = 0; i < bl.length; i++) {
      if (al.indexOf(bl[i]) === -1) {
        break;
      }
    }
    return flipCase(a, b.substr(0, i)) + b.substr(i);
  };
  return false;
}

// '1s'
GameManager.prototype.onesAddRandomTile = function () {
    let value = 1;
    if (this.moves > 12) {
      value = this.moves - 12;
    }
    let tile = new Tile(this.grid.randomAvailableCell(), value);
  
    this.grid.insertTile(tile);
};

let onesMergeResult = (a,b) => {
  if (a===b) return 1;
  return false;
}

// '👶🏻'
GameManager.prototype.babyAddRandomTile = function () {
  const exist1 = ['👶🏻', '🧒🏻', '👩🏻', '👩🏻‍🎓', '👩🏻‍🚀', '🚀👩🏻‍🚀', '🌑👩🏻‍🚀', '🌎👩🏻‍🚀', '👩🏻🥳'];
  const next1  = ['🍼',  '🥦', '🏫', '⛑', '🚀', '🌑',    '🔙',   '🏅',   '👨🏻'];
  const exist2 = ['👫', '🤰🏻', '🏃🏻‍♂️💨', '🤰🏻😢', '🤱🏻', '👩‍👦', '👩🏻👦🏻', '👋🏻👦🏻', '💥'];
  const next2  = ['🙊', '👨🏻', '💔',  '🕑',   '🕤', '🕙', '🎒',   '🚗',  '👩🏻😲'];
  const exist3 = ['👦🏻🤕', '🏥',  '👨🏻‍⚕️',   '👨🏻‍⚕️😞', '👩🏻😭', '👦🏻⚰', '👩🏻🤧', '🚶🏻‍♀️', '🐕❤'];
  const next3  = ['🚑',   '👩🏻⌛','👩🏻🙏🏻', '👩🏻😲​',   '🕧',    'F',    '🕝',   '🐕', '👩🏻​'];
  // Zero-Width space ahead:
  const exist4 = ['👩🏻🐕', '👩🏻🐕​', '👩🏻🐕​​', '👩🏻🐕​​​', '👩🏻🐕​​​​', '👩🏻🦀', '👩🏻‍🦲', '⚰'];
  const next4  = ['🕠',    '🕗',   '🕤',    '🕓',   '🦀',    '🏥​',   '⌛'];
  const exist = exist1.concat(exist2).concat(exist3).concat(exist4);
  const next = next1.concat(next2).concat(next3).concat(next4);
  if (this.grid.availableCells().length < this.size*this.size - 1) {
    return;
  }
  if (this.grid.hasTile('⚰')) return;
  for(let i = 0; i < exist.length; i++) {
    if (this.grid.hasTile(exist[i])) {
      let tile = new Tile(this.grid.randomAvailableCell(), next[i]);
      this.grid.insertTile(tile);
      break;
    }
  }
  if (this.grid.availableCells().length === this.size*this.size) {
    let tile = new Tile(this.grid.randomAvailableCell(), exist[0]);
    this.grid.insertTile(tile);
  }
};

let babyMergeResult = (a,b) => {
  const progression = ['👶🏻', '🧒🏻', '👩🏻', '👩🏻‍🎓', '👩🏻‍🚀', '🚀👩🏻‍🚀', '🌑👩🏻‍🚀', '🌎👩🏻‍🚀', '👩🏻🥳', '👫', '🤰🏻', '🏃🏻‍♂️💨',
    '🤰🏻😢', '🤱🏻', '👩‍👦', '👩🏻👦🏻', '👋🏻👦🏻', '💥', '👦🏻🤕', '🏥', '👨🏻‍⚕️', '👨🏻‍⚕️😞', '👩🏻😭', '👦🏻⚰', '👩🏻🤧', '🚶🏻‍♀️', '🐕❤',
    '👩🏻🐕', '👩🏻🐕​', '👩🏻🐕​​', '👩🏻🐕​​​', '👩🏻🐕​​​​', '👩🏻🦀', '👩🏻‍🦲', '⚰'];
  for (let i = 0; i < progression.length; i++) {
    if (a === progression[i]) return progression[i+1];
  }
  return false;
}

// '🐤'
GameManager.prototype.chickenAddRandomTile = function () {
  if (this.moves === 0) {
    let tile = new Tile(this.grid.randomAvailableCell(), '🐤');
    this.grid.insertTile(tile);
  } else if (this.moves === 1) {
    let tile = new Tile(this.grid.randomAvailableCell(), '🔪');
    this.grid.insertTile(tile);
  }
  this.moves += 1;
};

let chickenMergeResult = (a,b) => {
  return '🍗';
}

// '🥺'
GameManager.prototype.aloneAddRandomTile = function () {
  if (this.moves === 0) {
    let tile = new Tile(this.grid.randomAvailableCell(), '👀');
    this.grid.insertTile(tile);
  }
  this.moves += 1;
};

// '💿'
GameManager.prototype.cdAddRandomTile = function () {
  if (this.moves === 0) {
    let tile = new Tile(this.grid.randomAvailableCell(), 'C');
    this.grid.insertTile(tile);
  } else if (this.moves === 1) {
    let tile = new Tile(this.grid.randomAvailableCell(), 'D');
    this.grid.insertTile(tile);
  } else if (this.grid.hasTile('💿') || this.grid.hasTile('📀')) {
    let ccount = this.grid.countTile('C') + 1;
    let dcount = this.grid.countTile('D') + 1;
    let value = Math.random() < dcount / (ccount + dcount) ? 'C' : 'D';
    value = Math.random() < 0.9 ? value : 'K';
    if (!this.grid.hasTile('C') || !this.grid.hasTile('D') && value !== 'K') {
      if (!this.grid.hasTile('C') && value !== 'C')
        value = Math.random() < 0.5 ? 'D' : 'C';
      if (!this.grid.hasTile('D') && value !== 'D')
        value = Math.random() < 0.5 ? 'D' : 'C';
    }
    let tile = new Tile(this.grid.randomAvailableCell(), value);
    this.grid.insertTile(tile);
  }
  this.moves += 1;
};

let cdMergeResult = (a,b) => {
  if (a==='C' && b==='D') return '💿';
  if (a==='D' && b==='K') return '🐒';
  if (a===b && a==='💿') return '📀';
  return false;
}

// 'end'
GameManager.prototype.endAddRandomTile = function () {
  if (this.score === 0) {
    const msg = 'HANSFOPLAING';
    let tile = new Tile({x: (this.moves % 3), y: Math.floor(this.moves / 3)}, msg[this.moves]);
    this.moves += 1;
    this.grid.insertTile(tile);
  } else if (this.grid.availableCells().length > 0) {
    const msg = 'TKRY';
    const i = 4 - this.grid.availableCells().length;
    let tile = new Tile({x: 0, y: i}, msg[i]);
    this.grid.insertTile(tile);
  }
};
