# 2047

A twist on the classic 2048.

[Play it here](https://optionalgames.itch.io/2047).

The base code is "borrowed" from [Gabriele Cirulli](https://codepen.io/camsong/pen/wcKrg).
